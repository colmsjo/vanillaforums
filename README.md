Vanilla forum
=============

Contents:

 * vanilla-core-2-0-18-4.zip


This vanilla installation has been adapted in order to run on Heroku. See wp-config.php for the details.
There is also a wp-config.php.traditional which can be used on traditional servers (update with MySQL credentials
and rename to wp-config.php).

Heroku is setup in the following way (assuming the heroku command line tools are installed):

```
heroku create --stack cedar --remote production
heroku rename REPO-NAME # Or call it whatever you like for your project

heroku addons:add stillalive:basic
Adding stillalive:basic on mysterious-ravine-7314... done, v3 (free)
Thank you. Please log in to StillAlive via Heroku to setup your monitoring.
Use `heroku addons:docs stillalive:basic` to view documentation.

heroku addons:add cleardb:ignite     # Adds the MySQL option to the Heroku app's config
Adding cleardb:ignite on mysterious-ravine-7314... done, v4 (free)
Use `heroku addons:docs cleardb:ignite` to view documentation.

heroku config                        # See the URLs for your new databases
heroku config:add DATABASE_URL=mysql://... # Replace the "mysql://..." with the URL from CLEARDB_DATABASE_URL

#update cleardb-credentials with the new credentials
```

Update wp-config.php with:

 * random strings from here: https://api.wordpress.org/secret-key/1.1/salt/


Then commit and push to heroku!


Import the MySQL database

```
heroku config
mysql -u[USER] -p[PASSWORD] -h[HOST] [DATABASE] < mysql-data.sql
```

Login to Wordpress and goto the Settings->General

Change the Site Adress (URL) to www.gizur.se


Use the scripts in wordpress-heroku-tools (separate git repo from colmsjo) to load
the data into the MySQL database


